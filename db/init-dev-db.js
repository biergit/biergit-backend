db.createUser(
    {
        user: "bier",
        pwd: "git",
        roles: [
            {
                role: "readWrite",
                db: "biergit"
            }
        ]
    }
);

/** Init Biergit Database **/
biergitdb = db.getSiblingDB('biergit');
biergitdb.createCollection('users');
biergitdb.createCollection('groups');
biergitdb.createCollection('expenses');
