.PHONY: build deploy init

build:
	sh docker/build-image.sh

deploy:
	sh docker/deploy-image.sh

init:
	sh docker/init-image.sh
