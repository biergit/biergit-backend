package ch.juventus.biergit.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class BeerServiceTest {

    private static final String BEER_NAME = "biergit";

    @Autowired
    BeerService beerService;

    @DisplayName("Test Random Beer generation")
    @Test
    void testGet() {
        assertEquals(BEER_NAME, beerService.getRandomBeer().getName());
    }
}
