package ch.juventus.biergit.controller;

import ch.juventus.biergit.BiergitApplication;
import ch.juventus.biergit.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ActiveProfiles({"test"})
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = BiergitApplication.class
)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerTest {

    private static final MediaType MEDIA_TYPE_BIERGIT_V1 = new MediaType("application", "vnd.biergit.api.v1+json");
    private static final MediaType MEDIA_TYPE_JSON = new MediaType("application", "json");

    private final String CONTENT_BIERGIT_V1 = "application/vnd.biergit.api.v1+json";

    private MockMvc mockMvc;
    private List<User> testUsers = new ArrayList<>();

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) throws Exception {
        // setup mockmvc inc. documentation
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();

        // setup test data
        testUsers.clear();
        User user1 = new User("google|23232323", "Test", "User", "testi", "Test User", "test@user.com", "https://picutre.url");
        user1.setId("33424242424");
        User user2 = new User("microsoft|23232323", "Test", "User", "testi", "Test User", "test@ms.com", "https://picutre.url");
        user2.setId("4r4rwerw434353");

        testUsers.add(user1);
        testUsers.add(user2);
    }

    @Test
    public void v1_getCurrentLoggedInUser() throws Exception {
        mockMvc.perform(get("/users/me").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(status().isOk());
    }

    @Test
    public void latest_getCurrentLoggedInUser() throws Exception {
        mockMvc.perform(get("/users/me").accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void v1_getAllUsers(@Autowired MongoTemplate mongoTemplate) throws Exception {

        // save users to database
        testUsers.forEach(u -> mongoTemplate.remove(u, "users"));
        testUsers.forEach(u -> mongoTemplate.save(u, "users"));

        mockMvc.perform(get("/users").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(status().isOk());
    }

    @Test
    public void latest_getAllUsers(@Autowired MongoTemplate mongoTemplate) throws Exception {

        // save users to database
        testUsers.forEach(u -> mongoTemplate.remove(u, "users"));
        testUsers.forEach(u -> mongoTemplate.save(u, "users"));

        mockMvc.perform(get("/users").accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(status().isOk());
    }

    @Test
    public void v1_getAllUsersFilteredByName(@Autowired MongoTemplate mongoTemplate) throws Exception {

        // save users to database
        testUsers.forEach(u -> mongoTemplate.remove(u, "users"));
        testUsers.forEach(u -> mongoTemplate.save(u, "users"));

        mockMvc.perform(get("/users?fullName=Test").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(status().isOk());
    }

    @Test
    public void latest_getAllUsersFilteredByName(@Autowired MongoTemplate mongoTemplate) throws Exception {

        // save users to database
        testUsers.forEach(u -> mongoTemplate.remove(u, "users"));
        testUsers.forEach(u -> mongoTemplate.save(u, "users"));

        mockMvc.perform(get("/users?fullName=Test").accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(status().isOk());
    }

}
