package ch.juventus.biergit.controller;


import ch.juventus.biergit.BiergitApplication;
import ch.juventus.biergit.entity.Group;
import ch.juventus.biergit.entity.Transaction;
import ch.juventus.biergit.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ActiveProfiles({"test"})
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = BiergitApplication.class
)
@AutoConfigureMockMvc
public class GroupControllerTest {

    private static final MediaType MEDIA_TYPE_BIERGIT_V1 = new MediaType("application", "vnd.biergit.api.v1+json");
    private static final MediaType MEDIA_TYPE_JSON = new MediaType("application", "json");

    private MockMvc mockMvc;
    private List<User> testUsers = new ArrayList<>();
    private List<Group> testGroups = new ArrayList<>();

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation, @Autowired MongoTemplate mongoTemplate) throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();

        // setup test data
        testUsers.clear();
        User user1 = new User("google|23232323", "Test", "User", "testi", "Test User", "test@user.com", "https://picutre.url");
        user1.setId("33424242424");
        User user2 = new User("microsoft|23232323", "Test", "User", "testi", "Test User", "test@ms.com", "https://picutre.url");
        user2.setId("4r4rwerw434353");
        User user3 = new User("öpfel|23232323", "Test", "User", "testi", "Test User", "test@oep.com", "https://picutre.url");
        user3.setId("849524r4rwerw434353");

        testUsers.add(user1);
        testUsers.add(user2);
        testUsers.add(user3);

        Group group1 = new Group("JuveGroup",  user1, testUsers,"https://picture.url");
        group1.setId("948567324");
        Group group2 = new Group("HFGroup",  user2, testUsers,"https://picture.url");
        group2.setId("öjlaksdjfi34323");
        Group group3 = new Group("BeerGroup",  user3, testUsers,"https://picture.url");
        group3.setId("498573248752");

        group1.addMember(user1);

        testGroups.add(group1);
        testGroups.add(group2);
        testGroups.add(group3);

        // save groups to database
        testGroups.forEach(g -> mongoTemplate.dropCollection("groups"));
        testGroups.forEach(g -> mongoTemplate.save(g,"groups"));
    }

    @Test
    public void v1_getAllGroups(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/groups").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(3)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void v1_getAllGroupsFilteredByName(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/groups?groupName=Group").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(3)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void v1_getOneGroupsFilteredByName(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/groups?groupName=BeerGroup").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void v1_getOneGroupById(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/groups/" + testGroups.get(0).getId()).accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(testGroups.get(0).getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void v1_newGroup(@Autowired MongoTemplate mongoTemplate) throws Exception{
        Group g = new Group();
        g.setGroupName("Test Group");

        mockMvc.perform(MockMvcRequestBuilders.post("/groups")
                .content(new ObjectMapper().writeValueAsString(g))
                .contentType(MEDIA_TYPE_BIERGIT_V1)
                .accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void v1_addUserToGroup(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/groups/" + testGroups.get(0).getId() + "/members")
                .content(new ObjectMapper().writeValueAsString(new User(testUsers.get(0).getId())))
                .contentType(MEDIA_TYPE_BIERGIT_V1)
                .accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.members[0].id").value(testUsers.get(0).getId()))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void v1_removeUserFromGroup(@Autowired MongoTemplate mongoTemplate) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/groups/" + testGroups.get(0).getId() + "/members/" + testGroups.get(0).getMembers().get(0).getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}
