package ch.juventus.biergit.controller;

import ch.juventus.biergit.BiergitApplication;
import ch.juventus.biergit.dto.ExpenseDTO;
import ch.juventus.biergit.entity.*;
import ch.juventus.biergit.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ActiveProfiles({"test"})
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = BiergitApplication.class
)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExpensesControllerTest {

    private final Logger LOG = LoggerFactory.getLogger(ExpensesControllerTest.class);

    private static final MediaType MEDIA_TYPE_BIERGIT_V1 = new MediaType("application", "vnd.biergit.api.v1+json");
    private static final MediaType MEDIA_TYPE_JSON = new MediaType("application", "json");

    private MockMvc mockMvc;
    private List<ExpensePair> testExpenses = new ArrayList<>();
    private List<Group> testGroups = new ArrayList<>();
    List<User> testUsers = new ArrayList<>();

    @Autowired
    private UserService userService;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation, @Autowired MongoTemplate mongoTemplate) {

        // setup mockmvc inc. documentation
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();

        // clear global variables
        testExpenses.clear();
        testUsers.clear();
        testGroups.clear();

        // add current user to database
        User user1 = userService.getCurrentUser();
        testUsers.add(user1);

        // add dummy user to database
        User user2 = mongoTemplate.save(userService.createUser(userService.getDummyUser()), "users");
        testUsers.add(user2);

        // add dummy user to database
        User user3 = mongoTemplate.save(userService.createUser(userService.getDummyUser()), "users");
        testUsers.add(user3);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(new Transaction(user1, user2));
        transactions.add(new Transaction(user2, user1, 2));

        // add a direct expense pair (user1, user2)
        ExpensePair expensePair = new ExpensePair(user1, user2, new Context<>(Context.CONTEXT_DIRECT, user1.getId() + "_" + user2.getId()));
        expensePair.setTransactions(transactions);
        ExpensePair ep = mongoTemplate.save(expensePair, "expenses");
        testExpenses.add(ep);

        // add a new group with members (user1, user2, user3)
        Group group1 = new Group("Test Group",  user1, testUsers,"https://picture.url");
        Group g = mongoTemplate.save(group1, "groups");
        testGroups.add(g);

        // add a expense pair in context of a group (user1, user2)
        ExpensePair expensePairGroup = new ExpensePair(user1, user2, new Context<>(Context.CONTEXT_GROUP, testGroups.get(0).getId()));
        expensePairGroup.setTransactions(transactions);
        ExpensePair ep2 = mongoTemplate.save(expensePairGroup, "expenses");
        testExpenses.add(ep2);

    }

    @AfterEach
    public void cleanUp(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(this.testExpenses.get(0), "expenses");
        mongoTemplate.remove(this.testExpenses.get(1), "expenses");
        mongoTemplate.remove(this.testGroups.get(0), "groups");
        mongoTemplate.remove(this.testUsers.get(0), "users");
        mongoTemplate.remove(this.testUsers.get(1), "users");
        mongoTemplate.remove(this.testUsers.get(2), "users");
    }

    @Test
    public void v1_getAllMyExpensesFromOneGroup() throws Exception {
        mockMvc.perform(get("/groups/" + testGroups.get(0).getId()  + "/expenses").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk());
    }

    @Test
    public void v1_getAllMyExpenses() throws Exception {
        mockMvc.perform(get("/expenses").accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(jsonPath("$", hasSize(this.testExpenses.size())))
                .andExpect(status().isOk());
    }

    @Test
    public void latest_getAllMyExpenses() throws Exception {
        mockMvc.perform(get("/expenses").accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(jsonPath("$", hasSize(this.testExpenses.size())))
                .andExpect(status().isOk());
    }

    @Test
    public void v1_getAllTransactionsFromExpense() throws Exception {
        mockMvc.perform(
                get("/expenses/" + this.testExpenses.get(0).getId() + "/transactions")
                        .accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(jsonPath("$", hasSize(this.testExpenses.get(0).getTransactions().size())))
                .andExpect(status().isOk());
    }

    @Test
    public void latest_getAllTransactionsFromExpense() throws Exception {
        mockMvc.perform(
                get("/expenses/" + this.testExpenses.get(0).getId() + "/transactions")
                        .accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(jsonPath("$", hasSize(this.testExpenses.get(0).getTransactions().size())))
                .andExpect(status().isOk());
    }

    @Test
    public void v1_createTransactionForExpense() throws Exception {
        mockMvc.perform(
                post("/expenses/" + this.testExpenses.get(0).getId() + "/transactions")
                        .content(new ObjectMapper().writeValueAsString(new Transaction(
                                        userService.getCurrentUser(),
                                        userService.createUser(userService.getDummyUser())
                                ))
                        ).contentType(MEDIA_TYPE_BIERGIT_V1).accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(status().isCreated());
    }

    @Test
    public void latest_createTransactionForExpense() throws Exception {
        mockMvc.perform(
                post("/expenses/" + this.testExpenses.get(0).getId() + "/transactions")
                        .content(new ObjectMapper().writeValueAsString(new Transaction(
                                        userService.getCurrentUser(),
                                        userService.createUser(userService.getDummyUser())
                                ))
                        ).contentType(MEDIA_TYPE_JSON).accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void v1_createDirectExpense() throws Exception {
        mockMvc.perform(
                post("/expenses")
                        .content(new ObjectMapper().writeValueAsString(new ExpenseDTO(
                                        userService.getCurrentUser(),
                                        userService.createUser(userService.getDummyUser())
                                ))
                        ).contentType(MEDIA_TYPE_BIERGIT_V1).accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(status().isCreated());
    }

    @Test
    public void latest_createDirectExpense() throws Exception {
        mockMvc.perform(
                post("/expenses")
                        .content(new ObjectMapper().writeValueAsString(new ExpenseDTO(
                                        userService.getCurrentUser(),
                                        userService.createUser(userService.getDummyUser())
                                ))
                        ).contentType(MEDIA_TYPE_JSON).accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void v1_createGroupExpense() throws Exception {
        mockMvc.perform(
                post("/expenses")
                        .content(new ObjectMapper().writeValueAsString(new ExpenseDTO(
                                        testUsers.get(0),
                                        testUsers.get(1),
                                        new Context<>(Context.CONTEXT_GROUP, testGroups.get(0).getId())
                                ))
                        ).contentType(MEDIA_TYPE_BIERGIT_V1).accept(MEDIA_TYPE_BIERGIT_V1))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_BIERGIT_V1))
                .andExpect(status().isCreated());
    }

    @Test
    public void latest_createGroupExpense() throws Exception {

        mockMvc.perform(
                post("/expenses")
                        .content(new ObjectMapper().writeValueAsString(new ExpenseDTO(
                                        testUsers.get(0),
                                        testUsers.get(1),
                                new Context<>(Context.CONTEXT_GROUP, testGroups.get(0).getId())
                                ))
                        ).contentType(MEDIA_TYPE_JSON).accept(MEDIA_TYPE_JSON))
                .andDo(print())
                .andExpect(content().contentType(MEDIA_TYPE_JSON))
                .andExpect(status().isCreated());
    }


}
