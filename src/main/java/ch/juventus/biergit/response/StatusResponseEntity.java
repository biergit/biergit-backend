package ch.juventus.biergit.response;

import ch.juventus.biergit.dto.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Wrapper Class for the BaseError within a ResponseEntity
 */
public class StatusResponseEntity extends ResponseEntity<Status> {

    public StatusResponseEntity(HttpStatus httpStatus) {
        super(new Status(httpStatus), httpStatus);
    }

    public StatusResponseEntity(String link, HttpStatus httpStatus) {
        super(new Status(httpStatus, link), httpStatus);
    }

    public StatusResponseEntity(String message, HttpStatus httpStatus, String link) {
        super(new Status(httpStatus, message, link), httpStatus);
    }

}
