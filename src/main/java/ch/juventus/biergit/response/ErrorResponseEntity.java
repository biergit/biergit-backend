package ch.juventus.biergit.response;

import ch.juventus.biergit.error.BaseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

/**
 * Wrapper Class for the BaseError within a ResponseEntity
 */
public class ErrorResponseEntity extends ResponseEntity<BaseError> {

    public ErrorResponseEntity(HttpStatus httpStatus) {
        super(new BaseError(httpStatus), httpStatus);
    }

    public ErrorResponseEntity(String message, HttpStatus httpStatus) {
        super(new BaseError(httpStatus, message), httpStatus);
    }

    public ErrorResponseEntity(String message, HttpStatus httpStatus, List<String> errors) {
        super(new BaseError(httpStatus, message, errors), httpStatus);
    }

    public ErrorResponseEntity(String message, HttpStatus httpStatus, String error) {
        super(new BaseError(httpStatus, message, Collections.singletonList(error)), httpStatus);
    }
}
