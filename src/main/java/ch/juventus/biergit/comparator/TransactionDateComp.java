package ch.juventus.biergit.comparator;

import ch.juventus.biergit.entity.Transaction;

import java.util.Comparator;

public class TransactionDateComp implements Comparator<Transaction> {
    @Override
    public int compare(Transaction t1, Transaction t2) {

        if (t1.getTransactionDate().after(t2.getTransactionDate())) { return -1; }
        if (t1.getTransactionDate().before(t2.getTransactionDate())) { return 1; }

        return 0;
    }
}
