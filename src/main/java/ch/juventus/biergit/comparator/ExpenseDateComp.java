package ch.juventus.biergit.comparator;

import ch.juventus.biergit.entity.ExpensePair;

import java.util.Comparator;

public class ExpenseDateComp implements Comparator<ExpensePair> {
    @Override
    public int compare(ExpensePair e1, ExpensePair e2) {

        if (e1.getLastTransactionDate().after(e2.getLastTransactionDate())) { return -1; }
        if (e1.getLastTransactionDate().before(e2.getLastTransactionDate())) { return 1; }

        return 0;
    }
}
