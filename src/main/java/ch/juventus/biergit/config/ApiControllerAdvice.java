package ch.juventus.biergit.config;

import ch.juventus.biergit.controller.ExpensesController;
import ch.juventus.biergit.controller.GroupController;
import ch.juventus.biergit.controller.UserController;
import ch.juventus.biergit.json.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

@ControllerAdvice (
        assignableTypes = {
                ExpensesController.class,
                GroupController.class,
                UserController.class
        }
)
public class ApiControllerAdvice extends AbstractMappingJacksonResponseBodyAdvice {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer, MediaType contentType, MethodParameter returnType,
                                           ServerHttpRequest req, ServerHttpResponse res) {

        ServletServerHttpRequest request = (ServletServerHttpRequest) req;

        // Globally catch the view parameter and present specific views
        String view = request.getServletRequest().getParameter("view");

        LOG.debug("Cached parameters before sending to controller. View={}", view);

        if (view == null) view = "compact";

        LOG.debug("After caching the parameters before sending to controller. View={}", view);
        LOG.debug("Comparing parameter values to view objects: view={}", view);

        switch (view.toLowerCase()) {
            case "compact":
                bodyContainer.setSerializationView(View.Compact.class);
                LOG.debug("Set the compact view config");
                break;
            case "full":
                bodyContainer.setSerializationView(View.Full.class);
                LOG.debug("Set the full view config");
                break;
            default:
                bodyContainer.setSerializationView(View.Base.class);
                LOG.debug("Set the default (base) view config");
                break;
        }

        LOG.debug("Current applied views are [{}]", bodyContainer.getSerializationView());
    }
}
