package ch.juventus.biergit.error;

import ch.juventus.biergit.response.ErrorResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Environment env;

    // TODO: Implement global error handling functions

    @ExceptionHandler({ EntityNotFoundException.class })
    public ErrorResponseEntity handleEntityNotFoundException(
            EntityNotFoundException ex) {

        return new ErrorResponseEntity(ex.getMessage(),
                HttpStatus.NOT_FOUND, ex.getLocalizedMessage());
    }

    @ExceptionHandler({ InvalidExpenseMemberException.class })
    public ErrorResponseEntity handleInvalidExpenseMemberException(
            InvalidExpenseMemberException ex) {

        return new ErrorResponseEntity(ex.getMessage(),
                HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
    }

    // Return detailed error messages on local and dev environment
    @ExceptionHandler({ Exception.class })
    public ErrorResponseEntity handleAll(Exception ex, WebRequest request) {

        LOG.error(ex.getMessage());

        return new ErrorResponseEntity("The Server failed to proceed this request",
                HttpStatus.INTERNAL_SERVER_ERROR, getEnvErrorMessage(ex));
    }



    /**
     * Detects the environment over the env active profiles and returns the according error message details.
     * @param ex
     * @return String with error message
     */
    protected String getEnvErrorMessage(Exception ex) {

        for (final String profileName : env.getActiveProfiles()) {
            if (profileName.equals("dev") || profileName.equals("int") || profileName.equals("test")) {
                return ex.getLocalizedMessage();
            }
        }

        return "Something went wrong during request processing";
    }

}
