package ch.juventus.biergit.error;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BaseError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private ZonedDateTime timestamp;
    private HttpStatus status;
    private String message;

    @NonNull
    private List<String> errors;

    public BaseError(HttpStatus httpStatus) {
        this.status = httpStatus;
        this.message = httpStatus.getReasonPhrase();
        this.errors = new ArrayList<>();
        this.timestamp = this.getZonedDateTimeOfInstant();
    }

    public BaseError(HttpStatus httpStatus, String message) {
        this.status = httpStatus;
        this.message = message;
        this.errors = new ArrayList<>();
        this.timestamp = this.getZonedDateTimeOfInstant();
    }

    public BaseError(HttpStatus httpStatus, String message, List<String> errors) {
        this.status = httpStatus;
        this.message = message;
        this.errors = errors;
        this.timestamp = this.getZonedDateTimeOfInstant();
    }

    private ZonedDateTime getZonedDateTimeOfInstant() {
        Instant instant = Instant.now();
        ZoneId zoneId = ZoneId.of("Europe/Zurich");
        return ZonedDateTime.ofInstant( instant , zoneId );
    }
}
