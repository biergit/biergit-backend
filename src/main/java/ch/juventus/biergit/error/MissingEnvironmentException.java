package ch.juventus.biergit.error;

public class MissingEnvironmentException extends RuntimeException {

    public MissingEnvironmentException(String message) {
        super(message);
    }

}
