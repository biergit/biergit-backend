package ch.juventus.biergit.error;

public class InvalidExpenseMemberException extends RuntimeException {

    public InvalidExpenseMemberException(String message) {
        super(message);
    }

}
