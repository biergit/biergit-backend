package ch.juventus.biergit.repo;

import ch.juventus.biergit.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

// Query Syntax is described here -> https://docs.spring.io/spring-data/mongodb/docs/1.3.3.RELEASE/reference/html/mongo.repositories.html (Table 6.1)

public interface UserRepository extends MongoRepository<User, String> {
    User findByUserName(String userName);
    User findByEmail(String email);
    List<User> findByFullNameLike(String fullName);
}
