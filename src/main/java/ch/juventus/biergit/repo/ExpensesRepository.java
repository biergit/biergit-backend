package ch.juventus.biergit.repo;

import ch.juventus.biergit.entity.ExpensePair;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

// Query Syntax is described here -> https://docs.spring.io/spring-data/mongodb/docs/1.3.3.RELEASE/reference/html/mongo.repositories.html (Table 6.1)

public interface ExpensesRepository extends MongoRepository<ExpensePair, String> {

    @Query("{ 'context.reference' : '?0' }")
    List<ExpensePair> findAllByContextRef(String contextRef);

    @Query("{'$or':[ {'attendeeOne' : ?0}, {'attendeeTwo' : ?0} ] }")
    List<ExpensePair> findAllByUser(String userId);

    @Query("{ '$and' : [ { '$or':[ {'attendeeOne' : ?0}, {'attendeeTwo' : ?0 } ] } , { 'context.reference' : '?1' }]}")
    List<ExpensePair> findAllByUserAndContextRef(String userId, String contextRef);

    @Query("{ '$and' : [ { '$and':[ {'attendeeOne' : ?0}, {'attendeeTwo' : ?1 } ] } , { 'context.reference' : '?2' }]}")
    List<ExpensePair> findAllByUserFromAndToAndContextRef(String userOneId, String userTwoId, String contextRef);

}
