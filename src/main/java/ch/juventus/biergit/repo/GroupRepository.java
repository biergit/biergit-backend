package ch.juventus.biergit.repo;


import ch.juventus.biergit.entity.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;



public interface GroupRepository extends MongoRepository<Group, String> {

    List<Group> findByGroupNameLike(String groupName);

}
