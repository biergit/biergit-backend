package ch.juventus.biergit.dto;

import ch.juventus.biergit.entity.User;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class GroupDTO {

    private String id;

    @NotNull(message = "group name is mandatory")
    private String groupName;
    private User groupOwner;
    private List<User> members;
    private String pictureUrl;
    private Date importedAt;

    public GroupDTO(String groupName, User groupOwner) {
        this.groupName = groupName;
        this.groupOwner = groupOwner;
        this.importedAt = Calendar.getInstance().getTime();
    }

    public GroupDTO() {
        this.groupName = "";
        this.importedAt = Calendar.getInstance().getTime();
    }

}
