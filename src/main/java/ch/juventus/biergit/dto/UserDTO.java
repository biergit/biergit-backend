package ch.juventus.biergit.dto;


import lombok.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class UserDTO {

    @NotNull(message = "user id is mandatory")
    private String id;
    private String nickName;
    private String userName;
    private String firstName;
    private String lastName;
    private String fullName;
    private String email;
    private String pictureUrl;
    private Date importedAt;

    public UserDTO() {
        this.id = "";
    }
}
