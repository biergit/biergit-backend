package ch.juventus.biergit.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class Status {

    private int status;

    private String message;

    @NonNull
    private String link;

    public Status(HttpStatus httpStatus) {
        this.status = httpStatus.value();
        this.link = "";
    }

    public Status(HttpStatus httpStatus, String link) {
        this.status = httpStatus.value();
        this.link = link;
    }

    public Status(HttpStatus httpStatus, String message, String link) {
        this.status = httpStatus.value();
        this.link = link;
        this.message = message;
    }

}
