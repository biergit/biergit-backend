package ch.juventus.biergit.dto;

import ch.juventus.biergit.entity.Context;
import ch.juventus.biergit.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExpenseDTO {

    @NotNull(message = "userFrom is mandatory")
    private User userFrom;

    @NotNull(message = "userTo is mandatory")
    private User userTo;
    private Integer amount;
    private Context<String> context;

    // default amount of 1
    public ExpenseDTO(User userFrom, User userTo, Context<String> context) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = 1;
        this.context = context;
    }

    // default context
    public ExpenseDTO(User userFrom, User userTo, Integer amount) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = amount;
        this.context = new Context<>(Context.CONTEXT_DIRECT, userFrom.getId() + "_" + userTo.getId());
    }

    // default context and amount
    public ExpenseDTO(User userFrom, User userTo) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = 1;
        this.context = new Context<>(Context.CONTEXT_DIRECT, userFrom.getId() + "_" + userTo.getId());
    }
}
