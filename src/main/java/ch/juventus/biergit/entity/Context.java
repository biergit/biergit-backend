package ch.juventus.biergit.entity;

import ch.juventus.biergit.json.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Context<T> {

    public static final String CONTEXT_DIRECT = "direct";
    public static final String CONTEXT_GROUP = "group";

    @JsonView({View.Base.class})
    private String type;

    @JsonView({View.Base.class})
    private T reference;

    public Context(String type) {
        this.type = type;
    }
}
