package ch.juventus.biergit.entity;


import ch.juventus.biergit.json.View;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "users")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonView({View.Base.class})
    @Id
    private String id;

    @JsonIgnore
    @JsonView({View.Compact.class})
    private String nickName;

    @JsonIgnore
    private String userName;

    @JsonView({View.Compact.class})
    private String firstName;

    @JsonView({View.Compact.class})
    private String lastName;

    @JsonView({View.Base.class})
    private String fullName;

    @JsonView({View.Base.class})
    private String email;

    @JsonView({View.Compact.class})
    private String pictureUrl;

    @JsonView({View.Compact.class})
    private Date importedAt;

    public User(@NonNull String userName, String firstName, String lastName, String nickName, String fullName, @NonNull String email, String pictureUrl) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.fullName = fullName;
        this.email = email;
        this.pictureUrl = pictureUrl;
        this.importedAt = Calendar.getInstance().getTime();
    }

    public User(@NonNull String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
