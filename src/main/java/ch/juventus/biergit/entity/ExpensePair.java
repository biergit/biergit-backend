package ch.juventus.biergit.entity;

import ch.juventus.biergit.comparator.TransactionDateComp;
import ch.juventus.biergit.json.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.OrderBy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Document(value = "expenses")
public class ExpensePair {

    @Id
    @JsonView({View.Base.class})
    private String id;
    @NonNull
    @DBRef
    @JsonView({View.Base.class})
    private User attendeeOne;
    @NonNull
    @DBRef
    @JsonView({View.Base.class})
    private User attendeeTwo;
    @JsonView({View.Base.class})
    private Integer userFromDiff;
    @JsonView({View.Base.class})
    private Context context;

    @JsonView({View.Base.class})
    private Date lastTransactionDate;

    @JsonView({View.Full.class})
    @OrderBy("transactionDate")
    List<Transaction> transactions = new ArrayList<>();

    /**
     * Creates a new expense holder and adds a transaction to the default context "direct"
     *
     * @param attendeeOne Partner 1
     * @param attendeeTwo Partner 2
     * @param transaction Transaction
     */
    public ExpensePair(@NonNull User attendeeOne, @NonNull User attendeeTwo, Transaction transaction) {
        this.attendeeOne = attendeeOne;
        this.attendeeTwo = attendeeTwo;
        this.context = new Context<>("direct", attendeeOne + "_" + attendeeTwo);
        this.transactions.add(transaction);
    }

    /**
     * Creates a new expense holder and adds a transaction in a specific context
     *
     * @param attendeeOne Partner 1
     * @param attendeeTwo Partner 2
     * @param transaction Transaction
     */
    public ExpensePair(@NonNull User attendeeOne, @NonNull User attendeeTwo, Transaction transaction, Context<String> context) {
        this.attendeeOne = attendeeOne;
        this.attendeeTwo = attendeeTwo;
        this.context = context;
        this.transactions.add(transaction);
    }

    /**
     * Creates a new expense holder and adds a transaction in a specific context
     *
     * @param attendeeOne Partner 1
     * @param attendeeTwo Partner 2
     */
    public ExpensePair(@NonNull User attendeeOne, @NonNull User attendeeTwo, Context<String> context) {
        this.attendeeOne = attendeeOne;
        this.attendeeTwo = attendeeTwo;
        this.context = context;
    }

    /**
     * Creates an initial Expense based on the transaction infos
     *
     * @param transaction Transaction
     */
    public ExpensePair(Transaction transaction) {
        this.attendeeOne = transaction.getUserFrom();
        this.attendeeTwo = transaction.getUserTo();
        // TODO: Check first if the expense pair already exists
        this.transactions.add(transaction);
    }

    public void addTransaction(Transaction t) {
        if (this.transactions == null) {
            this.transactions = new ArrayList<>();
        }
        this.transactions.add(t);
    }

    public Boolean isAttendee(User user) {
        return user.equals(attendeeOne) || user.equals(attendeeTwo);
    }

    public Date getLastTransactionDate() {

        // check if transaction date is stored in database
        if (lastTransactionDate == null) {

            // the date is not stored and will be calculated
            if (transactions.size() > 0) {
                sortTransactionsByDate();
                return transactions.get(0).getTransactionDate();
            } else {
                try {
                    // old date, so the transaction will be the last one
                    lastTransactionDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-12-31");
                } catch (ParseException e) {
                    return new Date();
                }
            }

        }

        return lastTransactionDate;
    }

    public void sortTransactionsByDate()
    {
        Collections.sort(transactions, new TransactionDateComp());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExpensePair that = (ExpensePair) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}

