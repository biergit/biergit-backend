package ch.juventus.biergit.entity;


import ch.juventus.biergit.json.View;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@Document(collection = "groups")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Group {

    @JsonView({View.Base.class})
    @Id
    private String id;

    @JsonView({View.Base.class})
    private String groupName;

    @JsonView({View.Base.class})
    @DBRef
    private User groupOwner;

    @Transient
    @JsonView({View.Base.class})
    private Integer userFromDiff;

    @JsonView({View.Base.class})
    private Date lastTransactionDate;

    @DBRef
    @JsonView({View.Base.class})
    private List<User> members;

    @JsonView({View.Compact.class})
    private String pictureUrl;

    @JsonView({View.Full.class})
    private Date importedAt;

    public Group( String groupName, @NotNull User groupOwner, List<User> members,String pictureUrl) {
        this.groupName = groupName;
        this.groupOwner = groupOwner;
        this.members = members;
        this.pictureUrl = pictureUrl;
    }

    public Group() {
        this.importedAt = Calendar.getInstance().getTime();
        this.members = new ArrayList<>();
    }

    public Group(@NonNull String id) {
        this.id = id;
    }

    public void addMember(User user) {

        // check if the user still exists?
        Optional<User> u = this.members.stream().filter(m -> m.getId().equals(user.getId())).findFirst();

        // add user if not exists
        if (u.isEmpty()) {
            this.members.add(user);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

       Group group = (Group) o;

        return id.equals(group.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}

