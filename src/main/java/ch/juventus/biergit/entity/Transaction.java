package ch.juventus.biergit.entity;

import ch.juventus.biergit.json.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    @DBRef
    @JsonView({View.Base.class})
    private User userFrom;
    @NonNull
    @DBRef
    @JsonView({View.Base.class})
    private User userTo;
    @JsonView({View.Base.class})
    private Date transactionDate = new Date();
    @JsonView({View.Base.class})
    private Integer amount;

    /**
     * Creates a new Transaction with default settings
     * - amount = 1
     * - context = null
     * @param userFrom Who is the seeder?
     * @param userTo Who is the leecher?
     */
    public Transaction(@NonNull User userFrom, @NonNull User userTo) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = 1;
    }

    /**
     * Creates a transaction with default transaction date and id
     * @param userFrom Who is the seeder?
     * @param userTo Who is the leecher?
     * @param amount How much will be spent?
     */
    public Transaction(User userFrom, @NonNull User userTo, Integer amount) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = amount;
    }

}
