package ch.juventus.biergit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@SpringBootApplication
public class BiergitApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiergitApplication.class, args);
	}

}
