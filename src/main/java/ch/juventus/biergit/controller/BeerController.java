package ch.juventus.biergit.controller;

import ch.juventus.biergit.entity.Beer;
import ch.juventus.biergit.service.BeerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "hello")
public class BeerController {

    private final Logger LOG = LoggerFactory.getLogger(BeerController.class);
    private static final String CONTENT_TYPE_BIERGIT_V1 = "application/vnd.biergit.api.v1+json";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
        LOG.debug("inject services to beer controller");
    }

    @GetMapping(produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public RepresentationModel index() {
        RepresentationModel index = new RepresentationModel();
        index.add(linkTo(methodOn(BeerController.class).getBeer()).withSelfRel());
        return index;
    }

    @GetMapping(value = "beer", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity<Beer> getBeer() {
        LOG.debug("serving new beer to a client");
        return new ResponseEntity<>(beerService.getRandomBeer(), HttpStatus.OK);
    }

}
