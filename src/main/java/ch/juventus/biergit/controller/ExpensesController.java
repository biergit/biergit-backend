package ch.juventus.biergit.controller;

import ch.juventus.biergit.dto.ExpenseDTO;
import ch.juventus.biergit.entity.*;
import ch.juventus.biergit.response.ErrorResponseEntity;
import ch.juventus.biergit.json.View;
import ch.juventus.biergit.response.StatusResponseEntity;
import ch.juventus.biergit.service.ExpensesService;
import com.fasterxml.jackson.annotation.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping
public class ExpensesController {

    private final Logger LOG = LoggerFactory.getLogger(ExpensesController.class);
    private static final String CONTENT_TYPE_BIERGIT_V1 = "application/vnd.biergit.api.v1+json";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final MediaType MEDIA_TYPE_BIERGIT_V1 = new MediaType("application", "vnd.biergit.api.v1+json");

    private ExpensesService expensesService;

    @Autowired
    public ExpensesController(ExpensesService expensesService) {
        this.expensesService = expensesService;
        LOG.debug("inject expenses service to expenses controller");
    }

    @JsonView({View.Compact.class})
    @GetMapping(value = "/expenses", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getAllMyExpenses() {
        return new ResponseEntity<>(this.expensesService.getAllMyExpenses(), HttpStatus.OK);
    }

    @PostMapping(value = "/expenses", consumes = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON}, produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity addExpense(@RequestBody @Valid ExpenseDTO expense) {

        // create new expense (from me)
        User userFrom = expense.getUserFrom();
        User userTo = expense.getUserTo();

        // set default context for expense (direct)
        if (expense.getContext() == null) {
            expense.setContext(new Context<>(Context.CONTEXT_DIRECT));
        }

        // set default amount if not set
        if (expense.getAmount() == null) {
            expense.setAmount(1);
        }

        ExpensePair beforeChangeExpensePair = expensesService.getExpensePairByExpense(expense);

        // Add Transaction to Expense
        ExpensePair afterChangeExpensePair = expensesService.addTransactionToExpense(beforeChangeExpensePair.getId(), new Transaction(userFrom, userTo, expense.getAmount()));

        Link selfLink = linkTo(methodOn(ExpensesController.class).getTransactionByExpense(afterChangeExpensePair.getId())).withSelfRel();
        if (selfLink.getHref().isEmpty()) {
            return new ErrorResponseEntity("Failed to create expenses", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new StatusResponseEntity("Successfully created new Expense", HttpStatus.CREATED, selfLink.getHref());
        }
    }

    @GetMapping(value = "/expenses/{expenseId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getExpensePairById(@PathVariable String expenseId) {
        return new ResponseEntity<>(expensesService.getExpensePairById(expenseId), HttpStatus.OK);
    }

    //------ Group Expenses

    @GetMapping(value="/groups/{groupId}/expenses", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getAllMyExpensesFromGroup(@PathVariable String groupId) {
        return new ResponseEntity<>(expensesService.getAllMyExpensesInContextOfGroup(groupId), HttpStatus.OK);
    }

    //------ Transactions

    @PostMapping(value = "/expenses/{expenseId}/transactions", consumes = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON}, produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity addTransactionToExpense(@PathVariable String expenseId, @RequestBody Transaction transaction) {

        expensesService.addTransactionToExpense(expenseId, transaction);

        Link selfLink = linkTo(methodOn(ExpensesController.class).getTransactionByExpense(expenseId)).withSelfRel();
        if (selfLink.getHref().isEmpty()) {
            return new ErrorResponseEntity("Failed to create expenses", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new StatusResponseEntity("Successfully created new Transaction", HttpStatus.CREATED, selfLink.getHref());
        }

    }

    @GetMapping(value = "/expenses/{expenseId}/transactions", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity<List<Transaction>> getTransactionByExpense(@PathVariable String expenseId) {
        return new ResponseEntity<>(expensesService.getExpensePairById(expenseId).getTransactions(), HttpStatus.OK);
    }

}
