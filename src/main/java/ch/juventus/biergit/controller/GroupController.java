package ch.juventus.biergit.controller;

import ch.juventus.biergit.dto.GroupDTO;
import ch.juventus.biergit.dto.UserDTO;
import ch.juventus.biergit.entity.Group;
import ch.juventus.biergit.entity.User;
import ch.juventus.biergit.service.ExpensesService;
import ch.juventus.biergit.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "groups")
public class GroupController {
    private final Logger LOG = LoggerFactory.getLogger(GroupController.class);
    private static final String CONTENT_TYPE_BIERGIT_V1 = "application/vnd.biergit.api.v1+json";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private GroupService groupService;
    private ExpensesService expensesService;

    @Autowired
    public GroupController(GroupService groupService, ExpensesService expensesService) {
        this.groupService = groupService;
        this.expensesService = expensesService;
    }

    @GetMapping(produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity<List<Group>> getAllGroups(@RequestParam(required = false) String groupName) {
        if (groupName != null) {

            LOG.debug("Search database for user with groupName={}", groupName);

            return new ResponseEntity<>(this.groupService.getGroupLikeName(groupName), HttpStatus.OK);
        }

        List<Group> groups = this.groupService.getAllGroups();

        return new ResponseEntity<>(expensesService.appendCalcValues(groups), HttpStatus.OK);
    }

    @GetMapping(value = "/{groupId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getGroupById(@PathVariable String groupId) {

        LOG.debug("Search database for user with groupId={}", groupId);
        return new ResponseEntity<>(this.groupService.getGroupById(groupId), HttpStatus.OK);

    }

    @PostMapping(produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity createNewGroup(@RequestBody @Valid GroupDTO group) {

        if (group.getGroupName() == null || group.getGroupName().isBlank()) {
            return new ResponseEntity<>("Provide a valid group name", HttpStatus.BAD_REQUEST);
        }

        Group newGroup = new Group();
        newGroup.setGroupName(group.getGroupName());
        newGroup.setGroupOwner(group.getGroupOwner());
        newGroup.setMembers(group.getMembers());

        Group storedGroup = this.groupService.createGroup(newGroup);
        return new ResponseEntity<>(storedGroup, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{groupId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity updatedGroup(@PathVariable String groupId, @RequestBody GroupDTO group) {

        Group groupUpdate = new Group();
        groupUpdate.setGroupName(group.getGroupName());
        groupUpdate.setMembers(group.getMembers());
        groupUpdate.setGroupOwner(group.getGroupOwner());

        Group updatedGroup = this.groupService.updateGroup(groupId, groupUpdate);

        if (updatedGroup != null) {
            return new ResponseEntity<>(updatedGroup, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("failed to update group", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{groupId}/members/{userId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity deleteUserFromGroup(@PathVariable String groupId, @PathVariable String userId) {
        // TODO: Nochmals überprüfen ob der user sauber removed wird
        if (Boolean.TRUE.equals(this.groupService.deleteUserFromGroup(groupId, userId))) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>("could not remove user from group because of unknown reasons", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "/{groupId}/members", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity addUserToGroup(@PathVariable String groupId, @Valid @RequestBody UserDTO user) {

        User userToAdd = new User();
        userToAdd.setId(user.getId());

        Group updatedGroup = this.groupService.addUserToGroup(groupId, userToAdd);

        if (updatedGroup != null) {
            return new ResponseEntity<>(updatedGroup, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("failed to add member to group", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{groupId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity deleteUserGroup(@PathVariable String groupId) {
        this.groupService.deleteGroup(groupId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{groupId}/members", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getGroupMembers(@PathVariable String groupId) {

        List<User> members = this.groupService.getGroupMembers(groupId);

        LOG.debug("Search database for user with groupId={}", groupId);
        return new ResponseEntity<>(members, HttpStatus.OK);

    }
}
