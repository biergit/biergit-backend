package ch.juventus.biergit.controller;

import ch.juventus.biergit.entity.User;
import ch.juventus.biergit.response.ErrorResponseEntity;
import ch.juventus.biergit.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "users")
public class UserController {

    private final Logger LOG = LoggerFactory.getLogger(UserController.class);
    private static final String CONTENT_TYPE_BIERGIT_V1 = "application/vnd.biergit.api.v1+json";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
        LOG.debug("inject user service to user controller");
    }

    @GetMapping(produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity<List<User>> getAllUsers(@RequestParam(required = false) String fullName) {

        if (fullName != null) {
            LOG.debug("Search database for user with fullname={}", fullName);
            return new ResponseEntity<>(this.userService.getUsersByFullName(fullName), HttpStatus.OK);
        }

        return new ResponseEntity<>(this.userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "/{userId}", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getUserById(@PathVariable Integer userId) {
        if (userId > 0) {
            return new ErrorResponseEntity("this endpoint will be implemented soon", HttpStatus.OK);
        } else {
            return new ErrorResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/me", produces = {CONTENT_TYPE_BIERGIT_V1, CONTENT_TYPE_JSON})
    public ResponseEntity getMe() {

        User me = this.userService.getCurrentUser();

        if (me != null) {
            return new ResponseEntity<>(me, HttpStatus.OK);
        } else {
            return new ErrorResponseEntity("could not determine current logged in user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
