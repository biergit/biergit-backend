package ch.juventus.biergit.json;

public interface View {

    public interface Base {

    }

    // Base values and additional values for a compact view will be displayed
    public interface Compact extends Base {

    }

    // This View represents an full Entity
    public interface Full extends Compact {

    }
}
