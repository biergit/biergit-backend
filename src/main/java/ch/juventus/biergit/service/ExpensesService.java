package ch.juventus.biergit.service;

import ch.juventus.biergit.comparator.ExpenseDateComp;
import ch.juventus.biergit.dto.ExpenseDTO;
import ch.juventus.biergit.entity.*;
import ch.juventus.biergit.error.InvalidExpenseMemberException;
import ch.juventus.biergit.repo.ExpensesRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class ExpensesService {

    private final Logger LOG = LoggerFactory.getLogger(ExpensesService.class);

    private ExpensesRepository expensesRepository;
    private GroupService groupService;
    private UserService userService;

    @Autowired
    public ExpensesService(ExpensesRepository expensesRepository, UserService userService, GroupService groupService) {
        this.expensesRepository = expensesRepository;
        this.userService = userService;
        this.groupService = groupService;
    }

    // --------- Read Expenses

    /**
     * Get all Expenses where my user is attendeeOne or attendeeTwo
     *
     * @return a list of expense pairs
     */
    public List<ExpensePair> getAllMyExpenses() {
        User me = userService.getCurrentUser();
        LOG.debug("resolved current user as id={} full_name={}", me.getId(), me.getFullName());

        List<ExpensePair> expensePairs = this.getAllExpensesByUser(me);

        return rerangeUserFromInExpensePair(expensePairs, me);
    }

    /**
     * Make sure the current user is always the attendeeOne
     * @param expensePairs A list of expense pairs to rerange
     * @param attendeeOne The user which should be the attendeeOne
     * @return A reranged list of expense pairs
     */
    public List<ExpensePair> rerangeUserFromInExpensePair(List<ExpensePair> expensePairs, User attendeeOne) {

        // make sure the current user is always the attendeeOne
        for(int i = 0; i< expensePairs.size();i++) {
            ExpensePair ep = expensePairs.get(i);
            ep = sortExpensePairByUserAsAttendeeOne(attendeeOne, ep);
            expensePairs.set(i, ep);
        }

        return expensePairs;
    }

    /**
     * Get all Expenses where my user is attendeeOne or attendeeTwo in Context of a specific Group
     * @param groupId
     * @return
     */
    public List<ExpensePair> getAllMyExpensesInContextOfGroup(String groupId) {

        Group g = groupService.getGroupById(groupId);
        User me = userService.getCurrentUser();

        List<ExpensePair> expensePairs = expensesRepository.findAllByUserAndContextRef(me.getId(), g.getId());

        return rerangeUserFromInExpensePair(expensePairs, me);
    }

    /**
     * Get all Expenses where my user is attendeeOne or attendeeTwo in Context of a specific Group
     * @param groupId
     * @return
     */
    public List<ExpensePair> getExpensePairInContextOfGroup(String groupId, User userTo) {

        groupService.getGroupById(groupId);
        User userMe = userService.getCurrentUser();

        // check combinatin me, to, group
        List<ExpensePair> expensePairs = expensesRepository.findAllByUserFromAndToAndContextRef(userMe.getId(), userTo.getId(), groupId);

        // check combinatin to, me, group
        if (expensePairs.isEmpty()) {
            expensePairs = expensesRepository.findAllByUserFromAndToAndContextRef(userTo.getId(), userMe.getId(), groupId);
        }

        return rerangeUserFromInExpensePair(expensePairs, userMe);
    }

    /**
     * Get all Expenses where my user is attendeeOne or attendeeTwo in Context of a specific Group
     * @param groupId
     * @return
     */
    public List<ExpensePair> getAllInContextOfGroup(String groupId) {
        Group g = groupService.getGroupById(groupId);
        return expensesRepository.findAllByContextRef(g.getId());
    }

    /**
     * Returns all expenses of the according user
     *
     * @return List of Expenses
     */
    public List<ExpensePair> getAllExpensesByUser(User user) {
        List<ExpensePair> expensePairs = expensesRepository.findAllByUser(user.getId());
        LOG.debug("found {} expense pairs where user with user_id={}", expensePairs.size(), user.getId());
        return expensePairs;
    }

    /**
     * Accepts a expense object (simplified expense pair) and searches the according expense pair
     * @param e Simplified Expense Pair. DTO Object.
     * @return Expense Pair which match or null if not found
     */
    public ExpensePair getExpensePairByExpense(ExpenseDTO e) {

        ExpensePair ep = null;

        // check if this combination exists
        switch (e.getContext().getType().toLowerCase()) {
            case Context.CONTEXT_DIRECT:

                // check both variations
                String refOne = e.getUserFrom().getId() + "_" + e.getUserTo().getId();
                String refTwo = e.getUserTo().getId() + "_" + e.getUserFrom().getId();

                // check if already an expense pair with this context and this reference exists
                ep = getExpensePairByContextRef(refOne);
                if (ep == null) {
                    ep = getExpensePairByContextRef(refTwo);
                }

                // create a new one
                if (ep == null) {
                    ep = createExpensePairWithContext(e.getUserFrom(), e.getUserTo(), new Context<>("direct", refOne));
                }

                break;
            case Context.CONTEXT_GROUP:

                // check if already an expense pair with this context and this reference exists
                // TODO: This Check is not complete. Both, the attendeeOne and Two have to be checked!!
                List<ExpensePair> epList = getExpensePairInContextOfGroup(e.getContext().getReference(), e.getUserTo());

                if (epList.size() > 0) {
                    ep = epList.get(0);
                }

                if (ep == null) {
                    ep = createExpensePairWithContext(e.getUserFrom(), e.getUserTo(), new Context<>(Context.CONTEXT_GROUP, e.getContext().getReference()));
                }

                // update group summary values
                Group refGroup = groupService.getGroupById(ep.getContext().getReference().toString());
                refGroup.setLastTransactionDate(new Date());
                groupService.updateGroup(refGroup.getId(), refGroup);

                return ep;
            default:
                return getExpensePairByContextRef(e.getContext().getReference());
        }

        return ep;
    }

    public ExpensePair getExpensePairById(String expensePairId) {
        LOG.debug("search expenses with expense_pair_id={}", expensePairId);
        return expensesRepository.findById(expensePairId).orElseThrow(
                () -> new EntityNotFoundException("Could not find expense pair with id=" + expensePairId)
        );
    }

    public ExpensePair getExpensePairByContextRef(String ref) {
        LOG.debug("search expenses by context_reference={}", ref);
        List<ExpensePair> pairs = expensesRepository.findAllByContextRef(ref);
        if (pairs.size() > 0) {
            LOG.debug("found {} expenses with context_reference={}", pairs.size(), ref);
            return pairs.get(0);
        }
        return null;
    }

    // --------- Write Expenses

    public ExpensePair createExpensePair(User attendeeOne, User attendeeTwo) {
        return expensesRepository.save(new ExpensePair(attendeeOne, attendeeTwo));
    }

    public ExpensePair createExpensePairWithContext(User attendeeOne, User attendeeTwo, Context<String> context) {
        LOG.debug("create a new expense pair for attendee_one={} and attendee_two={} with context={}", attendeeOne.getId(), attendeeTwo.getId(), context);
        return expensesRepository.save(new ExpensePair(attendeeOne, attendeeTwo, context));
    }

    public ExpensePair addTransactionToExpense(String expensePairId, Transaction t) {

        ExpensePair ep = expensesRepository.findById(expensePairId).orElseThrow(
                () -> new EntityNotFoundException("could not find expense pair with id " + expensePairId)
        );

        // if the user does not match, the transaction is not allowed to be stored in this expense
        LOG.debug("check if the transaction has different users than the expense pair with expense_pair_id={}", expensePairId);
        if (Boolean.FALSE.equals(ep.isAttendee(t.getUserFrom())) && Boolean.FALSE.equals(ep.isAttendee(t.getUserTo()))) {
            throw new InvalidExpenseMemberException("seeder or leecher does not match with any attendee of the expense with id" + expensePairId);
        }

        LOG.debug("add transaction from_user={} to to_user={} with amount={} to expense with expense_pair_id={}",
                t.getUserFrom().getId(), t.getUserTo().getId(), t.getAmount(),  expensePairId);

        ep.addTransaction(t);
        ep.setLastTransactionDate(t.getTransactionDate());

        Integer totalSeedsOfAttendeeOne = 0;

        List<Transaction> transactions = new ArrayList<>(ep.getTransactions());

        // Update difference aggregation
        for (Transaction tr : transactions) {

            if (tr.getUserFrom().equals(ep.getAttendeeOne())) {
                // the attendee_one is the seeder
                totalSeedsOfAttendeeOne += tr.getAmount();
            } else {
                // the attendee_one is the leecher
                totalSeedsOfAttendeeOne -= tr.getAmount();
            }
        }

        ep.setUserFromDiff(totalSeedsOfAttendeeOne);

        return expensesRepository.save(ep);
    }

    public ExpensePair overrideExpensePair(ExpensePair expensePair) {
        return this.expensesRepository.save(expensePair);
    }

    // --------- Expense Utils

    public ExpensePair sortExpensePairByUserAsAttendeeOne(User attendeeOne, ExpensePair ep) {
        if (!ep.getAttendeeOne().equals(attendeeOne)) {
            User tmpUser = ep.getAttendeeOne();
            ep.setAttendeeOne(ep.getAttendeeTwo());
            ep.setAttendeeTwo(tmpUser);
            // invert amount calculation
            Integer totalSeedsOfAttendeeOne = ep.getUserFromDiff();
            totalSeedsOfAttendeeOne *= -1;
            ep.setUserFromDiff(totalSeedsOfAttendeeOne);
        }

        return ep;
    }

    /**
     * Calculates an aggregated value of the expenses from the group in context of a specific user
     * @param groupId the group id is required
     * @param user The user which will be the context
     * @return the sum of all expenses from the users perspective
     */
    public Integer sumGroupFromUser(String groupId, User user) {

        List<ExpensePair> groupExpenses = this.getAllInContextOfGroup(groupId);
        Integer userFromDiff = 0;

        for (ExpensePair ep : groupExpenses) {
            if (ep.isAttendee(user)) {
                if (ep.getUserFromDiff() != null) {
                    userFromDiff += ep.getUserFromDiff();
                }
            }
        }

        return userFromDiff;
    }

    /**
     * The last transaction date within this group
     * @param group
     * @return
     */
    public Date getLastTransactionDate(Group group) {

        if (group.getLastTransactionDate() != null) {
            return group.getLastTransactionDate();
        } else {
            List<ExpensePair> groupExpenses = this.getAllInContextOfGroup(group.getId());

            if (groupExpenses.size() > 0) {
                Collections.sort(groupExpenses, new ExpenseDateComp());
                return groupExpenses.get(0).getLastTransactionDate();
            }
        }

        return null;
    }

    public List<Group> appendCalcValues(List<Group> groups) {

        for (Group g : groups) {

            Integer userFromDiff = this.sumGroupFromUser(g.getId(), userService.getCurrentUser());

            if (g.getLastTransactionDate() == null) {
                Date lastTransactionDate =  getLastTransactionDate(g);
                g.setLastTransactionDate(lastTransactionDate);
            }

            g.setUserFromDiff(userFromDiff);

        }

        return groups;
    }

}
