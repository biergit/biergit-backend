package ch.juventus.biergit.service;

import ch.juventus.biergit.entity.Beer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BeerService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    public Beer getRandomBeer() {
        LOG.debug("generate new beer");
        return new Beer("biergit");
    }
}
