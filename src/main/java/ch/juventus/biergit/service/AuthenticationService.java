package ch.juventus.biergit.service;

import ch.juventus.biergit.entity.IdTokenUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class AuthenticationService {

    private final Logger LOG = LoggerFactory.getLogger(AuthenticationService.class);

    @Value("${spring.security.oauth2.resource.userInfoUri}")
    private String userInfoEndpoint;

    private RestTemplate restTemplate;

    public AuthenticationService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Obtains the user details from the userinfo endpoint.
     * @return IdTokenUser Entity with user data in it.
     */
    public IdTokenUser getIdTokenUserDetails() {

        // read access token from security context
        String accessToken = this.getAccessToken();

        if (accessToken != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + getAccessToken());
            headers.set("Content-Type", "application/json");
            headers.set("User-Agent", "biergit-api/1.0.0v Biergit REST API v1");
            HttpEntity<String> request = new HttpEntity<>( null, headers);

            ResponseEntity<IdTokenUser> result = this.restTemplate.exchange(this.userInfoEndpoint, HttpMethod.GET, request, IdTokenUser.class);

            if (result.getStatusCode() == HttpStatus.OK) {
                return result.getBody();
            } else {
                LOG.error("Failed to obtain user info from {} because of HTTP Code: {}", userInfoEndpoint, result.getStatusCodeValue());
            }

        }

        return null;
    }

    /**
     * Obtains the access token from spring security authentication holder.
     * @return access_token in JWT format
     */
    public String getAccessToken() {

        if (getAuthentication() instanceof JwtAuthenticationToken) {
            JwtAuthenticationToken jwtAuth = (JwtAuthenticationToken) getAuthentication();
            return jwtAuth.getToken().getTokenValue();
        } else {
            return null;
        }
    }

    public String getUniqueUserId() {
        return getAuthentication().getName();
    }

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
