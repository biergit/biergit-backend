package ch.juventus.biergit.service;


import ch.juventus.biergit.comparator.ExpenseDateComp;
import ch.juventus.biergit.entity.ExpensePair;
import ch.juventus.biergit.entity.Group;
import ch.juventus.biergit.entity.User;
import ch.juventus.biergit.repo.GroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class GroupService {

    private final Logger LOG = LoggerFactory.getLogger(GroupService.class);

    private GroupRepository groupRepository;
    private UserService userService;


    @Autowired
    public GroupService(GroupRepository groupRepository, UserService userService) {
        this.groupRepository = groupRepository;
        this.userService = userService;
    }

    public List<Group> getAllGroups() {
        return this.groupRepository.findAll();
    }

    public Group getGroupById(String id) {
        return this.groupRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("could not find group with id " + id));
    }

    public List<Group> getGroupLikeName(String groupName) {
        return this.groupRepository.findByGroupNameLike(groupName);
    }

    public List<User> getGroupMembers(String groupId) {
        Optional<Group> m = this.groupRepository.findById(groupId);
        if(m.isPresent()){
            return m.get().getMembers();
        }else{
            throw new EntityNotFoundException("group with id " + groupId + " could not be found");
        }
    }

    public Group createGroup(String groupName, User user, List<User> members, String pictureUrl) {
        Group group = new Group(groupName, user, members, pictureUrl);
        LOG.debug("crating new Group {}", group.getGroupName());
        return this.groupRepository.save(group);
    }

    public Group createGroup(Group group) {
        LOG.debug("crating new group {}", group.getGroupName());

        group.setImportedAt(new Date());

        // check if owner is provided, default is me
        if (group.getGroupOwner() == null) {
            group.setGroupOwner(this.userService.getCurrentUser());
        }

        User me = this.userService.getCurrentUser();

        // check if the requester is member of the group, when not, add the current user as member
        if (!group.getMembers().isEmpty()) {
            if (!group.getMembers().contains(me)) {
                group.getMembers().add(me);
            }
        } else {
            group.getMembers().add(me);
        }

        return this.groupRepository.save(group);
    }

    public Group addUserToGroup(String groupId, User user) {
        Optional<Group> o = this.groupRepository.findById(groupId);
        if (o.isPresent()) {
            Group group = o.get();
            group.getMembers().add(user);
            this.groupRepository.save(group);
            return group;
        } else {
            throw new EntityNotFoundException("group with id " + groupId + " could not be found");
        }

    }

    public Boolean deleteUserFromGroup(String groupId, String userId) {

        if (groupId != null && !groupId.isEmpty() && userId != null && !userId.isEmpty()) {
            Optional<Group> o = this.groupRepository.findById(groupId);

            if (o.isPresent()) {
                Optional<User> user = o.get().getMembers().stream().filter(m -> m.getId().equals(userId)).findFirst();
                if (user.isPresent()) {
                    o.get().getMembers().remove(user.get());
                    this.groupRepository.save(o.get());
                }
                return true;
            } else {
                throw new EntityNotFoundException("user can not be removed because group with id "+groupId+" does not exists.");
            }

        }
        return false;
    }

    public void deleteGroup(String groupId) {
        Optional o = this.groupRepository.findById(groupId);
        if (o.isPresent()) {
            this.groupRepository.deleteById(groupId);
        } else {
            LOG.debug("group with id {} does not exist", groupId);
        }
    }

    public Group updateGroup(String groupId, Group currentGroup) {

        Optional<Group> o = this.groupRepository.findById(groupId);
        if (o.isPresent()) {
            Group group = o.get();
            group.setGroupName(currentGroup.getGroupName());
            group.setGroupOwner(currentGroup.getGroupOwner());
            group.setMembers(currentGroup.getMembers());
            group.setPictureUrl(currentGroup.getPictureUrl());
            group.setLastTransactionDate(currentGroup.getLastTransactionDate());

            return groupRepository.save(group);
        } else {
            throw new EntityNotFoundException("group with id " + groupId + " could not be found");
        }

    }

    /* --------- Utils -------------- */


}


