# [DEPRECATED] Biergit Backend

[![CircleCI](https://circleci.com/bb/biergit/biergit-backend/tree/master.svg?style=svg)](https://circleci.com/bb/biergit/biergit-backend/tree/master)

The Biergit Backend is a Spring Boot REST Application. 

Spring Boot Version: `2.2.0.RELEASE`

## Environment Prerequisites

* Oracle or Open JDK Version 11+
* Running Docker deamon
* Gradle 5.3.1 (alternatively,  the wrapper can be used)

## Project Setup

```
# start the development services`
$ docker-compose up

# start the application with gradle
$ gradle bootrun
```

## Access Database
The docker compose file will start two containers. First is the mongo-database. The second container is a mongo-db express ui to access the data.

The `Database UI` accessible at http://localhost:8081 or use a `Mongo DB Client` Software like https://robomongo.org

```
host: localhost
port: 27017
admin-user: root
admin-password: biergit1234
```

## Run the application
After a build was done via gradle build, the application can be started. 
```
$ java -jar -Dspring.profiles.active=dev build/libs/biergit-backend-0.0.1-SNAPSHOT.jar

// Set SPRING_ACTIVE_PROFILES Env Variable to desired environemnt and start
$ java -jar build/libs/biergit-backend-0.0.1-SNAPSHOT.jar
```

## Read the Docs
The Backend provides a http://localhost:8080/docs endpoint with all the relevant stuff.

## Setup IDE

You have to enable the `annotation processing` from intelliJ. 
![Annotation Processing](assets/idea/lombok.png)

## Run Tests
```
$ ./gradlew clean test
```

## Run  Build (with testing)
```
$ ./gradlew clean build
```

## Create a Java Package (without testing)
```
$ ./gradlew clean assemble
```

## Build Docker image
```
$ ./gradlew docker
```

## Publish Docker image
```
$ ./gradlew dockerPush
```

## Manual build and publish Dockerimage
```
$ docker build -t biergit/backend-poc .
$ docker tag biergit/backend-poc eu.gcr.io/biergit-poc/api:v0.0.6
$ docker push eu.gcr.io/biergit-poc/api:v0.0.6
```

## Deploy to Kubernetes
```
$ kubectl apply -f kubernetes/bgt-api-deployment.yaml
```

## Deploy to Cloud Foundry
These commands should be run from the root directory of this project.
```
# build the application (jar file has to be present in build/libs)
$ gradle build

# push the application by pre-configured manifest files
$ cf push -f manifests/manifest-{env}.yml

# push to integration
$ cf push -f manifests/manifest-int.yml

# push to production
$ cf push -f manifests/manifest-prod.yml
```

